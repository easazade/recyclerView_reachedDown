package info.devexchanges.endlessrecyclerview.endlessrecyclerview2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.Arrays;

import info.devexchanges.endlessrecyclerview.R;

public class EndlessActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_endless);

    String[] arrayString = {"reza","hasan","sasan","saman","yashar",
      "haman","rashid","raha","saman","khaled","samane","sara","soosasn","nazgol"};

    RecyclerView recyclerView = findViewById(R.id.endlessActivity_recyclerView);
    MyRecyclerAdapter adapter =
      new MyRecyclerAdapter(recyclerView, Arrays.asList(arrayString),this);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(adapter);
    adapter.serOnListReachedBottomListener(new MyRecyclerAdapter.OnListReachedBottomListener() {
      @Override
      public void onReachedBottom() {
        Toast.makeText(EndlessActivity.this,"reached down",Toast.LENGTH_LONG).show();
      }
    });
    //TODO this aproach is easier than the first one note they have different use cases as
    //TODO first approach is better for making apps like Divar or FaceBook

  }
}
