package info.devexchanges.endlessrecyclerview.endlessrecyclerview2;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import info.devexchanges.endlessrecyclerview.R;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyItemViewHolder> {

  List<String> items;
  LayoutInflater inflater;
  OnListReachedBottomListener onReachedBottomListener;

  public MyRecyclerAdapter(RecyclerView recyclerView, List<String> items, Activity activity) {
    this.items = items;
    this.inflater = LayoutInflater.from(activity);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        boolean reachedBottom = !recyclerView.canScrollVertically(1);
        if (reachedBottom) {
          if (onReachedBottomListener != null)
            onReachedBottomListener.onReachedBottom();
        }
      }
    });
  }

  @NonNull
  @Override
  public MyItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = inflater.inflate(R.layout.simple_item,parent,false);
    return new MyItemViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull MyItemViewHolder holder, int position) {
    holder.textView.setText(items.get(position));
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  class MyItemViewHolder extends RecyclerView.ViewHolder {

    public TextView textView;

    public MyItemViewHolder(View itemView) {
      super(itemView);
      textView = itemView.findViewById(R.id.simpleItem_textView);
    }
  }

  public void serOnListReachedBottomListener(OnListReachedBottomListener listener) {
    this.onReachedBottomListener = listener;
  }

  public interface OnListReachedBottomListener {
    void onReachedBottom();
  }


}
