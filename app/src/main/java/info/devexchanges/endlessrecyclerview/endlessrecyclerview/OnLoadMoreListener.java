package info.devexchanges.endlessrecyclerview.endlessrecyclerview;

public interface OnLoadMoreListener {
    void onLoadMore();
}